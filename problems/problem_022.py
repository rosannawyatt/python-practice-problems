# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_sunny is False and is_workday is True:
        gear.append("umbrella")
    if is_workday is True:
        gear.append("laptop")
    if is_workday is False:
        gear.append("surfboard")
    return gear

Monday = True
Tuesday = True
Wednesday = True
Saturday = False
Sunday = False

Sunny = True
Cloudy = False
Raining = False

print(gear_for_day(Monday, Sunny))
print(gear_for_day(Monday, Cloudy))
print(gear_for_day(Sunday, Sunny))
print(gear_for_day(Saturday, Raining))