# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(number):
    numbers = [1,2,3,4,5,6,7,8,9,10]
    roman_num = ["I","II","III","IV","V","VI","VII","VIII","IX","X"]
    dictionary = dict(zip(numbers, roman_num))
    return dictionary.get(number)

print(simple_roman(1))
print(simple_roman(2))
print(simple_roman(3))
print(simple_roman(4))
print(simple_roman(5))
print(simple_roman(6))
print(simple_roman(7))
print(simple_roman(8))
print(simple_roman(9))
print(simple_roman(10))

# OR you could just write out the dictionary