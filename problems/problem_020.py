# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if attendees_list >= (members_list / 2):
        return True
    else:
        return False

print(has_quorum(51, 100))
print(has_quorum(49, 100))
print(has_quorum(50, 100))
print(has_quorum(54, 99))

# OR

def has_quorum(attendees_list, members_list):
    num_attendees = len(attendees_list)
    num_members = len(members_list)
    if num_attendees >= (num_members * 0.5):
        return True
    else:
        return False
    
members = ("Rita", "Donny", "Lamar", "Amina", "Lyzel"
           "Ray", "Star", "Angela", "Asia", "Cho")
atendees = ("Lyzel", "Donny", "Amina", "Rita")
members2 = ("Rita", "Donny", "Lamar", "Amina", "Lyzel"
           "Ray", "Star", "Angela", "Asia", "Cho")
atendees2 = ("Lyzel", "Donny", "Amina", "Rita", "Cho")

print(has_quorum(atendees, members))
print(has_quorum(atendees2, members2))
