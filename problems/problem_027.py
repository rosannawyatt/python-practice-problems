# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    max = values[0]
    for num in values:
        if num > max:
            max = num
    return max

values1 = [8, 2, 5,99, 72, -4]
values2 = [-9, 0, -34, -1]
values3 = []

print(max_in_list(values1))
print(max_in_list(values2))
print(max_in_list(values3))