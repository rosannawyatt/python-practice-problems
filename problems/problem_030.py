# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    largest = values[0]
    second_largest = values[1]
    for value in values:
        if value > largest:
            second_largest = largest
            largest = value
        elif value > second_largest and value != largest:
            second_largest = value
    return second_largest

# OR

# def find_second_largest(values):
#     if len(values) <= 1:
#         return None
#     sorted_list = []
#     for item in values:
#         if item not in sorted_list:
#             sorted_list.append(item)
#     sorted_list.sort()
#     return sorted_list[-2]

list1 = [1, 2, 3, 4, 5]
list2 = [5, 67, 2, -8, 1000]
list3 = [1000, 55, -2, 67]

print(find_second_largest(list1))
print(find_second_largest(list2))
print(find_second_largest(list3))

