# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

# create empty variable for each condition

# for char in password:
#     if char.isalpha:
#         variable = True
    
# same for digit, upper, lower, special char

# if all variables and len(password) >= 6 and len(password) <= 12:
# return True

def check_password(password):
    alpha = False
    digit = False
    upper = False
    lower = False
    special = False
    error = "Try password again"
    for char in password:
        if char.isalpha():
            alpha = True
    for char in password:
        if char.isdigit():
            digit = True
    for char in password:
        if char.isupper():
            upper = True
    for char in password:
        if char.islower():
            lower = True
    for char in password:
        if char == "$" or "!" or "@":
            special = True
    if (alpha and digit and upper and lower and special 
        and len(password) >= 6 and len(password) <=12):
        return True
    else:
        return error

print(check_password("Pa$sword1"))
print(check_password("Password"))
print(check_password("Pa$sword"))
print(check_password("Pa$1"))