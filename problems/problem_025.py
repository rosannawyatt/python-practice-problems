# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum = 0
    if len(values) == 0:
        return None
    for item in values:
        sum += item
    return sum

values = [1,2,3,4,5,6]
values1 = []

print(calculate_sum(values))
print(calculate_sum(values1))